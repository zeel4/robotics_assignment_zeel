#!/usr/bin/env python

import rospy
import math
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
import sys
import numpy as np


# from turtlesim.srv import (Spawn, SpawnRequest)


class ReachGoal:

    def __init__(self, goal_x, goal_y):
        self.xp = None
        self.yp = None
        self.yaw = None
        self.line_vel = None
        self.ang_vel = None
        self.pub = None
        self.r = rospy.Rate(100)

        self.kp_l = 1
        self.kd_l = 0.1
        self.ki_l = 0.0001

        self.kp_a = 2
        self.kd_a = 0.1
        self.ki_a = 0.001

        self.chase_distance = 0
        self.distance_int = 0
        self.yaw_int = 0
        self.yaw_diff = 0
        self.goal_x = goal_x
        self.goal_y = goal_y
        self.record_x = []
        self.record_y = []
        self.chase_distance_record = np.array([])
        self.init_sub_pub()

    def init_sub_pub(self):
        rospy.Subscriber('turtle1/pose', Pose, self._cb_input)
        self.pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=1)

    def _cb_input(self, req):
        self.xp = req.x
        self.yp = req.y
        self.record_x.append(self.xp)
        self.record_y.append(self.yp)
        self.yaw = req.theta
        self.line_vel = req.linear_velocity
        self.ang_vel = req.angular_velocity
        self._reach_goal()

    def _reach_goal(self):
        command = Twist()
        command.linear.x = self._set_linear_speed()
        command.angular.z = self._set_angular_speed()
        if self.chase_distance >= 0.3:
            self.pub.publish(command)
        self.r.sleep()
        self.save_numpy()

    def _set_linear_speed(self):
        self.chase_distance = math.sqrt((self.xp - self.goal_x) ** 2 + (self.yp - self.goal_y) ** 2)
        self.chase_distance_record = np.append(self.chase_distance_record, self.chase_distance)
        self.distance_int = self.distance_int + self.chase_distance
        command_linear_speed = (self.kp_l * self.chase_distance) + ((-self.kd_l) * self.line_vel) + (self.ki_l *
                                                                                                     self.distance_int)
        return command_linear_speed

    def _set_angular_speed(self):
        target_rad = math.atan2(self.goal_y - self.yp, self.goal_x - self.xp)
        self.yaw_diff = target_rad - self.yaw

        if math.degrees(self.yaw_diff) >= 180:
            self.yaw_diff = self.yaw_diff - 6.2832
        if math.degrees(self.yaw_diff) <= -180:
            self.yaw_diff = self.yaw_diff + 6.2832

        command_angular_speed = (self.kp_a * self.yaw_diff) + ((-self.kd_a) * self.ang_vel)
        return command_angular_speed

    def save_numpy(self):
        print('Saved')
        if self.chase_distance <= 0.3:
            np.save('Chased_istance', self.chase_distance_record)


def main():
    rospy.init_node('TBotGoal1')
    rospy.loginfo("[TBotGoal1] initialized")
    x = float(sys.argv[1])
    y = float(sys.argv[2])
    obj = ReachGoal(x, y)

    try:
        print('Spining')
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo('[TBotGoal1] closed')


if __name__ == '__main__':
    main()
