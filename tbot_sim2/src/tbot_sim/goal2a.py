#!/usr/bin/env python

import rospy
import math
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
# from std_srvs.srv import (Empty, EmptyRequest)
from turtlesim.srv import (Spawn, SpawnRequest)
import sys


class ReachGoal2:

    def __init__(self, goal_x, goal_y, params):
        self.xp = None
        self.yp = None
        self.yaw = None
        self.line_vel = None
        self.ang_vel = None
        self.pub = None
        self.r = rospy.Rate(100)

        self.parameters = params

        self.linear_acc_limit = (1, -1)
        self.angular_acc_limit = (1, -1)

        self.chase_distance = 0
        self.distance_int = 0
        self.yaw_int = 0
        self.yaw_diff = 0
        self.goal_x = goal_x
        self.goal_y = goal_y
        self.record_x = []
        self.record_y = []
        self.record_linear_speed = []
        self.record_angular_speed = []
        self.n_iter = 100

        self.init_sub_pub()

    def init_sub_pub(self):
        rospy.Subscriber('turtle1/pose', Pose, self._cb_input)
        self.pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=1)

    def _cb_input(self, req):
        self.xp = req.x
        self.yp = req.y
        self.record_x.append(self.xp)
        self.record_y.append(self.yp)
        self.yaw = req.theta
        self.line_vel = req.linear_velocity
        self.record_linear_speed.append(self.line_vel)
        self.ang_vel = req.angular_velocity
        self.record_angular_speed.append(self.ang_vel)
        self._reach_goal(xp=self.xp, yp=self.yp, yaw=self.yaw, line_vel=self.line_vel, ang_vel=self.ang_vel)

    def _reach_goal(self, xp=None, yp=None, yaw=None, line_vel=None, ang_vel=None):
        command = Twist()
        command.linear.x = self._set_linear_speed(xp=xp, yp=yp, line_vel=line_vel)
        command.angular.z = self._set_angular_speed(yaw=yaw, xp=xp, yp=yp, ang_vel=ang_vel)
        if self.chase_distance >= 0.3:
            self.pub.publish(command)
        self.r.sleep()

    def _set_linear_speed(self, xp=None, yp=None, line_vel=None):
        self.chase_distance = math.sqrt((xp - self.goal_x) ** 2 + (yp - self.goal_y) ** 2)
        self.distance_int = self.distance_int + self.chase_distance
        command_linear_speed = (self.parameters[0] * self.chase_distance) + ((-self.parameters[2]) * line_vel) + \
                               (self.parameters[1] * self.distance_int)
        linear_acc = (command_linear_speed - self.record_linear_speed[-1]) / 0.01

        if linear_acc > self.linear_acc_limit[0]:
            linear_acc = self.linear_acc_limit[0]
            command_linear_speed = (linear_acc * 0.01) + self.record_linear_speed[-1]

        if linear_acc < self.linear_acc_limit[1]:
            linear_acc = self.linear_acc_limit[1]
            command_linear_speed = (linear_acc * 0.01) + self.record_linear_speed[-1]

        return command_linear_speed

    def _set_angular_speed(self, yaw=None, xp=None, yp=None, ang_vel=None):
        target_rad = math.atan2(self.goal_y - yp, self.goal_x - xp)
        self.yaw_diff = target_rad - yaw
        self.yaw_int = self.yaw_int + self.yaw_diff

        if math.degrees(self.yaw_diff) >= 180:
            self.yaw_diff = self.yaw_diff - 6.2832
        if math.degrees(self.yaw_diff) <= -180:
            self.yaw_diff = self.yaw_diff + 6.2832

        command_angular_speed = (self.parameters[3] * self.yaw_diff) + ((-self.parameters[5]) * ang_vel) + \
                                (self.parameters[4] * self.yaw_int)
        angular_acc = (command_angular_speed - self.record_angular_speed[-1]) / 0.01

        if angular_acc > self.angular_acc_limit[0]:
            angular_acc = self.angular_acc_limit[0]
            command_angular_speed = (angular_acc * 0.01) + self.record_angular_speed[-1]

        if angular_acc < self.angular_acc_limit[1]:
            angular_acc = self.angular_acc_limit[1]
            command_angular_speed = (angular_acc * 0.01) + self.record_angular_speed[-1]
        return command_angular_speed


def main():
    rospy.init_node('TBotGoal2')
    rospy.loginfo("[TBotGoal2] initialized")
    # call_reset = rospy.ServiceProxy('/spawn', Spawn)
    # req = SpawnRequest()
    # req.name = 'newlife1'
    # req.x = 1.0
    # req.y = 1.0
    # req.theta = 0
    # response  = call_reset(req)
    params = [1, 0.0001, 0.8, 2, 0.0001, 0.8]
    goal_points = [[10, 1], [10, 2]]
    x = float(sys.argv[1])
    y = float(sys.argv[2])
    obj = ReachGoal2(x, y, params)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo('[TBotGoal2] closed')


if __name__ == '__main__':
    main()
