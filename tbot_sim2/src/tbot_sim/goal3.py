#!/usr/bin/env python

import rospy
import math
import random
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from tbot_sim2.msg import PoseData


class MoveCircle:

    def __init__(self, speed, radius):
        self.xp = None
        self.yp = None
        self.yaw = None
        self.line_vel = None
        self.ang_vel = None
        self.publish_vel = None
        self.publish_pose = None
        self.publish_noisy_pose = None
        self.command_linear_speed = speed
        self.command_angular_speed = None
        self.radius = radius
        self.command = Twist()
        self.pose_vals = PoseData()
        self.pose_noisy_vals = PoseData()

        self.init_sub_pub()
        self.t1 = rospy.Timer(rospy.Duration(5), self._publish_poses, oneshot=False)
        self.t2 = rospy.Timer(rospy.Duration(0.01), self._move_circle, oneshot=False)
        self.t3 = rospy.Timer(rospy.Duration(5), self._publish_noisy_pose, oneshot=False)

    def init_sub_pub(self):
        rospy.Subscriber('turtle1/pose', Pose, self._cb_input)
        self.publish_vel = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=1)
        self.publish_pose = rospy.Publisher('/rt_real_pose', PoseData, queue_size=1)
        self.publish_noisy_pose = rospy.Publisher('/rt_noisy_pose', PoseData, queue_size=1)

    def _cb_input(self, req):
        self.xp = req.x
        self.yp = req.y
        self.yaw = req.theta
        self.line_vel = req.linear_velocity
        self.ang_vel = req.angular_velocity

    def _move_circle(self, event):
        self.command_angular_speed = self.command_linear_speed / self.radius
        self.command.linear.x = self.command_linear_speed
        self.command.angular.z = self.command_angular_speed
        self.publish_vel.publish(self.command)

    def _publish_poses(self, event):
        self.pose_vals.x = self.xp
        self.pose_vals.y = self.yp
        self.pose_vals.theta = self.yaw
        self.pose_vals.linear_velocity = self.line_vel
        self.pose_vals.angular_velocity = self.ang_vel

        self.publish_pose.publish(self.pose_vals)

    def _publish_noisy_pose(self, event):
        self.pose_noisy_vals.x = self.xp + random.gauss(0, 0.5)
        self.pose_noisy_vals.y = self.yp + random.gauss(0, 0.5)
        self.pose_noisy_vals.theta = self.yaw + random.gauss(0, 0.5)
        self.pose_noisy_vals.linear_velocity = self.line_vel + random.gauss(0, 0.5)
        self.pose_noisy_vals.angular_velocity = self.ang_vel + random.gauss(0, 0.5)

        self.publish_noisy_pose.publish(self.pose_noisy_vals)


def main():
    rospy.init_node('TBotGoal3')
    rospy.loginfo("[TBotGoal3] initialized")
    obj = MoveCircle(0.5, 4)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo('[TBotGoal3] closed')


if __name__ == '__main__':
    main()
