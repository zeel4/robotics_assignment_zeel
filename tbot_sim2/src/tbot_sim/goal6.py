#!/usr/bin/env python

import rospy
import math
import numpy as np
import matplotlib.pyplot as plt
import message_filters
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from turtlesim.srv import (Spawn, SpawnRequest)
from tbot_sim2.msg import PoseData


class ReachGoal6:

    def __init__(self):
        self.xp = 0
        self.yp = 0
        self.yaw = 0
        self.line_vel = 0
        self.ang_vel = 0
        self.pub = None
        self.pub_est = None
        self.pose = PoseData()
        self.r = rospy.Rate(100)

        self.linear_acc_limit = (5, -5)
        self.angular_acc_limit = (math.radians(500), math.radians(-500))
        self.speed_limit = 0.5

        self.kp_l = 60
        self.kd_l = 3
        self.ki_l = 0.000

        self.kp_a = 50
        self.kd_a = 15
        self.ki_a = 0.00

        self.chase_distance = 10
        self.distance_int = 0
        self.yaw_int = 0
        self.yaw_diff = 0
        self.goal_x = 5
        self.goal_x_record = []
        self.goal_y = 5
        self.goal_y_record = []
        self.goal_theta = 0
        self.goal_line_vel = 0
        self.record_linear_speed = [0]
        self.record_angular_speed = [0]

        self.X = np.array([[5], [5], [0], [0]])
        self.x_estimate_record = []
        self.y_estimate_record = []
        self.A = np.array([[1, 0, 5, 0],
                           [0, 1, 0, 5],
                           [0, 0, 1, 0],
                           [0, 0, 0, 1]])
        self.P = np.array([[0.5, 0, 0, 0],
                           [0, 0.5, 0, 0],
                           [0, 0, 0.5, 0],
                           [0, 0, 0, 0.5]])
        self.C = np.array([[1, 0, 5, 0],
                           [0, 1, 0, 5],
                           [0, 0, 1, 0],
                           [0, 0, 0, 1]])
        self.R = np.array([[0.5, 0, 0, 0],
                           [0, 0.5, 0, 0],
                           [0, 0, 0.25, 0],
                           [0, 0, 0, 0.25]])
        self.K = None
        self.Y = None

        self.init_sub_pub()
        self.t1 = rospy.Timer(rospy.Duration(0.01), self._reach_goal, oneshot=False)

    def init_sub_pub(self):
        pb = message_filters.Subscriber('/posedata', PoseData)
        rb = message_filters.Subscriber('/rt_noisy_pose', PoseData)
        self.pub = rospy.Publisher('newlife1/cmd_vel', Twist, queue_size=1)
        self.pub_est = rospy.Publisher('/estimation', PoseData, queue_size=1)

        ts = message_filters.TimeSequencer([pb, rb], 1)
        ts.registerCallback(self._cb_input)

    def _cb_input(self, PolicePose, RobberPose):
        self.xp = PolicePose.x
        print ('Police pose : ', self.xp)
        self.yp = PolicePose.y
        self.yaw = PolicePose.theta
        self.line_vel = PolicePose.linear_velocity
        self.record_linear_speed.append(self.line_vel)
        self.ang_vel = PolicePose.angular_velocity
        self.record_angular_speed.append(self.ang_vel)

        self.goal_x = RobberPose.x
        self.goal_x_record.append(self.goal_x)
        self.goal_y = RobberPose.y
        self.goal_y_record.append(self.goal_y)

        self.goal_theta = RobberPose.theta
        self.goal_line_vel = RobberPose.linear_velocity

        self.Y = np.array([[self.goal_x], [self.goal_y], [self.goal_line_vel * math.cos(self.goal_theta)],
                           [self.goal_line_vel * math.sin(self.goal_theta)]])

        self._estimate_pose()

    def _reach_goal(self, event):
        command = Twist()

        command.linear.x = self._set_linear_speed()
        command.angular.z = self._set_angular_speed()
        if self.chase_distance >= 0.3:
            self.pub.publish(command)

    def _set_linear_speed(self):
        self.chase_distance = math.sqrt((self.xp - self.goal_x) ** 2 + (self.yp - self.goal_y) ** 2)
        print('Chase Distance : ', self.chase_distance)
        self.distance_int = self.distance_int + self.chase_distance
        command_linear_speed = (self.kp_l * self.chase_distance) + ((-self.kd_l) * self.line_vel) + (self.ki_l *
                                                                                                     self.distance_int)

        linear_acc = (command_linear_speed - self.record_linear_speed[-1]) / 0.01

        if linear_acc > self.linear_acc_limit[0]:
            linear_acc = self.linear_acc_limit[0]
            command_linear_speed = (linear_acc * 0.01) + self.record_linear_speed[-1]

        if linear_acc < self.linear_acc_limit[1]:
            linear_acc = self.linear_acc_limit[1]
            command_linear_speed = (linear_acc * 0.01) + self.record_linear_speed[-1]

        if command_linear_speed > self.speed_limit:
            print('Out of speed limit')
            command_linear_speed = self.speed_limit

        return command_linear_speed

    def _set_angular_speed(self):
        target_rad = math.atan2(self.goal_y - self.yp, self.goal_x - self.xp)
        self.yaw_diff = target_rad - self.yaw
        print('Yaw diff: ', math.degrees(self.yaw_diff))
        self.yaw_int = self.yaw_int + self.yaw_diff

        if math.degrees(self.yaw_diff) >= 180:
            self.yaw_diff = self.yaw_diff - 6.2832
        if math.degrees(self.yaw_diff) <= -180:
            self.yaw_diff = self.yaw_diff + 6.2832

        command_angular_speed = (self.kp_a * self.yaw_diff) + ((-self.kd_a) * self.ang_vel) + (self.ki_a * self.yaw_int)

        angular_acc = (command_angular_speed - self.record_angular_speed[-1]) / 0.01

        if angular_acc > self.angular_acc_limit[0]:
            angular_acc = self.angular_acc_limit[0]
            command_angular_speed = (angular_acc * 0.01) + self.record_angular_speed[-1]

        if angular_acc < self.angular_acc_limit[1]:
            angular_acc = self.angular_acc_limit[1]
            command_angular_speed = (angular_acc * 0.01) + self.record_angular_speed[-1]

        return command_angular_speed

    def _estimate_pose(self):
        print ('Estimation')
        self.X = np.matmul(self.A, self.X)

        self.P = np.matmul(self.A, self.P)
        self.P = np.matmul(self.P, self.A.T)
        self.P = np.diag(np.diag(self.P))

        self.K = np.divide(self.P, self.P + self.R)
        self.K = np.nan_to_num(self.K)

        self.Y = np.matmul(self.C, self.Y)

        self.X = self.X + np.matmul(self.K, self.Y - self.X)
        self.x_estimate_record.append(self.X[0][0])
        self.y_estimate_record.append(self.X[1][0])
        self.P = np.matmul((np.identity(4) - self.K), self.P)
        self.goal_x = self.X[0][0]
        self.goal_y = self.X[1][0]

        self.pose.x = self.goal_x
        self.pose.y = self.goal_y

        self.pub_est.publish(self.pose)


def main():
    rospy.init_node('TBotGoal6')
    rospy.loginfo("[TBotGoal6] initialized")

    call_reset = rospy.ServiceProxy('/spawn', Spawn)
    req = SpawnRequest()
    req.name = 'newlife1'
    req.x = 1.0
    req.y = 1.0
    req.theta = 0
    response = call_reset(req)

    obj = ReachGoal6()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo('[TBotGoal6] closed')


if __name__ == '__main__':
    main()
