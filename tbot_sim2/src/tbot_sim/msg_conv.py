#!/usr/bin/env python
import rospy
import math
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from tbot_sim2.msg import PoseData


class MessageConverter:

    def __init__(self):
        self.xp = None
        self.yp = None
        self.yaw = None
        self.line_vel = None
        self.ang_vel = None
        self.pub = None
        self.command = PoseData()
        self.init_sub_pub()

    def init_sub_pub(self):
        rospy.Subscriber('/newlife1/pose', Pose, self._cb_input)
        self.pub = rospy.Publisher('/posedata', PoseData, queue_size=1)

    def _cb_input(self, req):
        self.xp = req.x
        self.yp = req.y
        self.yaw = req.theta
        self.line_vel = req.linear_velocity
        self.ang_vel = req.angular_velocity
        self._publish_data()

    def _publish_data(self):
        self.command.x = self.xp
        self.command.y = self.yp
        self.command.theta = self.yaw
        self.command.linear_velocity = self.line_vel
        self.command.angular_velocity = self.ang_vel
        self.pub.publish(self.command)


def main():
    rospy.init_node('Message_converter')
    rospy.loginfo("[TBotGoal1] initialized")
    obj = MessageConverter()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo('[TBotGoal1] closed')


if __name__ == '__main__':
    main()
